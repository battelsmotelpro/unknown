<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'theme\PageController@index')->name('index');
Route::get('/logout', 'theme\MemberController@setLogout');
Auth::routes();
Route::get('login/{provider}', 'Login\SocialController@redirect');
Route::get('login/{provider}/callback','Login\SocialController@Callback');

Route::group(['middleware' => ['users']], function(){
    
Route::resource('/real-estate', 'theme\RealEstateController');	
Route::get('/ilan-detay/{id}', 'theme\PageController@estate_detail');
Route::get('/hakkimizda', 'theme\PageController@aboutUs');	
Route::get('/iletisim', 'theme\PageController@contact');	
Route::get('/ilanlarim/{id}', 'theme\RealEstateController@show');
Route::get('/favori-ilanlarim', 'theme\PageController@favorite_estates');
Route::get('/profil', 'theme\ProfileController@profil')->name('profil');
Route::post('/profile/update', 'theme\ProfileController@update')->name('profilupdate');
Route::post('/profile/block/{id}', 'theme\ProfileController@userBlock')->name('userBlock');
// Shering
Route::post('/estate-shering/{id}', 'theme\RealEstateController@estateShering')->name('estateShering');
// Favorites
Route::post('/estate-favorite/{id}', 'theme\RealEstateController@estateFavorite')->name('estateFavorite');
// Like
Route::post('/estate-like/{id}', 'theme\RealEstateController@estateLike')->name('estateLike');
// Comments
Route::post('/comments', 'theme\CommentController@store')->name('comment');
Route::post('/comment/{id}', 'theme\CommentController@destroy')->name('commentDestroy');
Route::post('/comment/{id}/edit', 'theme\CommentController@update');
// Search
Route::get('/search', 'theme\SearchController@search')->name('search');
Route::post('/filter', 'theme\SearchController@filter')->name('filter');
// takip ve takibi bırakma ile ilgili routelar
Route::post('/follow/{userId}', 'theme\MemberController@followUser')->name('follow');
Route::post('/unfollow/{userId}', 'theme\MemberController@unfollowUser')->name('unfollow');
Route::get('/profile/mesajlar', 'theme\ProfileController@messages')->name('profilmesajlar');

Route::post('/sendmessage', 'theme\ProfileController@sendmessage')->name('sendmessage');

});

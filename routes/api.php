<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/shipments', 'theme\ShipmentController@shipmentsapi');
Route::get('/shipments/{id}', 'theme\ShipmentController@shipmentapi');
Route::post('/shipments', 'theme\ShipmentController@storeapi');
Route::put('/shipments/{id}', 'theme\ShipmentController@updateapi');
Route::delete('/shipments/{id}', 'theme\ShipmentController@deleteapi');
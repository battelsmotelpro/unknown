@extends('layouts.main')

@section('styles')
<style type="text/css">
       <style>
      body{
      background:#f5f8fa
      }
      @media (min-width: 1200px) { .container {
          max-width: 1200px;
      } }
      </style>
</style>
@endsection


@section('content')

  <div class="container" style="padding:3%;padding-left: 7%;padding-right: 7%;">
      
      <div class="row">
       
        <div class="col-8"> 
        


          <div class="card mb-3">
          	 @if(Session::has('success'))
              <div class="alert alert-success" role="alert">
                 {{ Session::get('success')}}
              </div>
              @endif
              <div class="card-header">
                Paylaşım
              </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              
              <div class="card gedf-card">
                  <div class="card-header">
                      <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                              <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Durum</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Döküman</a>
                          </li>
                      </ul>
                  </div>
                  <div class="card-body">
                      <form action="/shipments" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }} 
                      <div class="tab-content" id="myTabContent">
                          <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                          <input type="hidden" name="type_id" value="0">
                          <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                              <div class="form-group">
                                  <label class="sr-only" for="message">post</label>
                                  <textarea class="form-control mb-3" id="message" name="title" rows="3" placeholder="Neler Oluyor ?"></textarea>
                                  <div class="custom-file">
                                      <input type="file" required="" name="image" class="custom-file-input" id="customFile">
                                      <label class="custom-file-label" for="customFile">Resim Yükle</label>
                                  </div>
                              </div>
                              <button type="submit" class="btn btn-primary btn-sm">Gönder</button>
                          </div>
                        </form> 


                           
                          <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                            
                              <form action="/documents" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                            <input type="hidden" name="type_id" value="1">
                              <div class="form-group">
                                  <div class="custom-file ">
                                      <input type="file" name="file" class="custom-file-input" id="customFile">
                                      <label class="custom-file-label" for="customFile">Dosya Ekle</label>
                                  </div>
                              </div>
                              <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
                                  </div>
                                  <select name="faculty_id" class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seçiniz...</option>
                                    @foreach($faculties as $faculty)
                                    <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
                                    </div>
                                    <select name="chapter_id" class="custom-select" id="inputGroupSelect01">
                                      <option selected>Seçiniz...</option>
                                     
                                    </select>
                                  </div>
                                  <div class="input-group mb-3">
                                      <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
                                      </div>
                                      <select name="class_id" class="custom-select" id="inputGroupSelect01">
                                        <option selected value="0">Seçiniz...</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="3">4</option>
                                      </select>
                                    </div>

                                    <textarea class="form-control" name="description" id="message" rows="3" placeholder="Açıklama..."></textarea>
                              <div class="py-4"></div>
                               <button type="submit" class="btn btn-primary btn-sm">Gönder</button>
                             </form>
                          </div>
                      </div>
                     
                  </div>
              </div>
              
              
            </div>


          @foreach($currentuser->followings as $key=>$follow)

            @foreach($follow->shipments as $shipment)        
             <div class="card mb-3">
                <div class="card-header text-right">
                      <button class="btn  dropdown-toggle dropdown dropdown-menu-left btn-group-sm " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="" aria-expanded="false">
                     {{$shipment->user->name}}
                      </button>
                    
                    
                </div>
    
                <div class="card-body">
           	@if($shipment->media==null && $shipment->image == null)
              <div class="card-body">
           
                
              <p class="card-text"> <span class="glyphicon glyphicon-pencil" ></span>{{$shipment->title}}</p>
          
            </div>

             @endif
              @if(($shipment->media))
                        <object style="    width: 100%; height:200px;;zoom: 3;" data="media/{{$shipment->media}}" type="application/pdf">
                                     <embed src="/media/{{$shipment->media}}" type="application/pdf" />
                        </object>
                          <p class="card-text"> <span class="glyphicon glyphicon-pencil" ><i class="fa fa-search"></i></span>{{$shipment->description}}</p>
              @endif

              @if(isset($shipment->image))
                      <div id="carouselExampleSlidesOnly" class="carousel slide mb-3" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                              <img src="/images/shipments/{{$shipment->image}}" class="d-block w-100" alt="...">
                          </div>
                        </div>
                      </div>
                        <p class="card-text"> <span class="glyphicon glyphicon-pencil" ></span>{{$shipment->title}}</p>
              @endif  
            
             
                
                </div>
                <div class="card-footer text-muted">
                 
                  <div class="btn-group" role="group" style="width: 100%;" aria-label="Basic example">
                    
                    
                     <button type="button"  data-toggle="collapse" href="#collapseExample"  class="btn btn-success">Yorum Yap</button>
                     <form action="{{ route('shipmentShering', ['id' => $shipment->id]) }}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Paylaş</button>
                    </form>
                  </div>
    
                </div>
               <div class="collapse" id="collapseExample">
                <div class="card-footer text-muted">
                 
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="button" id="button-addon2">Yorum Yap</button>
                    </div>
                  </div>
    
                </div>
                <div class="card-footer text-muted">
                 
                yorumlar liste
                </div>
              </div>
            </div>
            @endforeach
            @endforeach


          @foreach($shipments as $key=>$shipment)
            <div class="card mb-3">
                <div class="card-header text-right">
                  
    
                  
                      <button class="btn  dropdown-toggle dropdown dropdown-menu-left btn-group-sm " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="" aria-expanded="false">
                       ...
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item"  data-toggle="modal" data-target="#duzenleModal" href="#">Düzenle</a>
                        <a class="dropdown-item" href="{{ route('delete-shipment', ['id' => $shipment->id]) }}" onclick="return confirm('Silmek istediğinize emin misiniz?');">Sil</a>
                      </div>
                    
                </div>
    
                <div class="card-body">
             @if($shipment->media==null && $shipment->image == null)
              <div class="card-body">
           
                
              <p class="card-text"> <span class="glyphicon glyphicon-pencil" ></span>{{$shipment->title}}</p>
          
            </div>

             @endif
              @if(($shipment->media))
                        <object style="    width: 100%; height:200px;;zoom: 3;" data="media/{{$shipment->media}}" type="application/pdf">
                                     <embed src="/media/{{$shipment->media}}" type="application/pdf" />
                        </object>
                          <p class="card-text"> <span class="glyphicon glyphicon-pencil" ><i class="fa fa-search"></i></span>{{$shipment->description}}</p>
              @endif

              @if(isset($shipment->image))
                      <div id="carouselExampleSlidesOnly" class="carousel slide mb-3" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                              <img src="/images/shipments/{{$shipment->image}}" class="d-block w-100" alt="...">
                          </div>
                        </div>
                      </div>
                        <p class="card-text"> <span class="glyphicon glyphicon-pencil" ></span>{{$shipment->title}}</p>
              @endif  
            
             
                
                </div>
                <div class="card-footer text-muted">
                 
                  <div class="btn-group" role="group" style="width: 100%;" aria-label="Basic example">
                     @php $countLike = App\ShipmentLike::where('shipment_id',$shipment->id)->where('user_id',$currentuser->id)->count() @endphp
                    
                    @php $like = App\ShipmentLike::where('shipment_id',$shipment->id)->where('user_id',$currentuser->id)->first() @endphp
                    @if($like)
                   
                    <button  type="text" class="btn btn-success">
                        <span class="fa fa-thumbs-up" ></span>
                        Beğendin <i>({{$countLike}})</i>
                    </button>
                   

                    @else
                    <form action="/shipment-like/{{$shipment->id}}" method="post">
                    {{ csrf_field() }}
                    <button  type="submit" class="btn btn-success">
                        <span class="fa fa-thumbs-up" ></span>
                        Beğen <i>({{$countLike}})</i>
                    </button>
                    </form>
                    @endif
                    <button type="button"  data-toggle="collapse" href="#shipmentscomments{{$key+1}}"  class="btn btn-success">Yorum Yap</button>
                    <form action="{{ route('shipmentShering', ['id' => $shipment->id]) }}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Paylaş</button>
                    </form>
                  </div>
    
                </div>
               <div class="collapse" id="shipmentscomments{{$key+1}}">
                <div class="card-footer text-muted">
                 <!-- Yorum Yap -->
                 <form action="{{ route('comment')}}" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                  <input type="hidden" name="shipment_id" value="{{$shipment->id}}">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="description" placeholder="Yorumunuz..." aria-label="Yorumunuz..." aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Yorum Yap</button>
                    </div>
                  </div>
                  </form>
                  <!-- Yorum Yap Son -->
                </div>
                <div class="card-footer text-muted">
                 
                  <!-- Yorumlar -->
                    <div class="card mb-3">
                    @php $comments = App\Comment::where('shipment_id',$shipment->id)->get() @endphp
                    @if(isset($comments))
                    @forelse($comments as $comment)
                     
                    <!-- Yorum 1 -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    
                                   

                                </div>
                                <div class="col-md-10">
                                    <p>
                                       
                                   </p>
                                   <div class="clearfix"></div>
                                    <p>{{$comment->description}}</p>
                                </div>
                            </div>
                        </div>
                         <!-- Yorum 1 Son -->
                         <hr>
                   
                       
                    @empty
                           <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                   
                                    <img src="/assets/img/newuserlogo.png" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                                   

                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <a class="float-left" href="#"><strong>Hiç Yorum Eklenmemiş</strong></a>
                                   </p>
                                   <div class="clearfix"></div>
                                    <p>İlk Yorumu siz ekleyin</p>
                                </div>
                            </div>
                        </div>
                         

                    @endforelse
                    @endif
                    </div>
                </div>
              </div>
            </div>
            @endforeach


        </div>
        <div class="col-4">
        
            <div class="card mb-3">
                <h5 class="card-header">Döküman Arama Sistemi</h5>
                <form action="/document-search" method="get">
                <div class="card-body">
                
                    <div class="input-group">
                        <div class="input-group mb-3">
                        <div class="input-group-prepend">
                         <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
                        </div>
                        <select name="faculty_id" class="custom-select" id="inputGroupSelect01">
                        <option selected>Seçiniz...</option>
                        @foreach($faculties as $faculty)
                            <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                        @endforeach
                        </select>
                        </div>
                        <div class="input-group mb-3">
                         <div class="input-group-prepend">
                           <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
                         </div>
                         <select name="chapter_id" class="custom-select" id="inputGroupSelect01">
                           <option selected>Seçiniz...</option>
                           
                         </select>
                        </div>
                        <div class="input-group mb-3">
                           <div class="input-group-prepend">
                             <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
                           </div>
                           <select name="class_id" class="custom-select" id="inputGroupSelect01">
                             <option selected>Seçiniz...</option>
                             <option value="1">1</option>
                             <option value="2">2</option>
                             <option value="3">3</option>
                             <option value="3">4</option>
                           </select>
                         </div>
                     
                        </div>

                       <button type="submit" class="btn btn-primary btn-sm">Listele</button>
                </div>
              </form>
              </div>

                 <!-- Önerilen Kullanıcılar --> 
              <div class="card mb-3">
                <h5 class="card-header">Önerilen Kullanıcılar</h5>
                <div class="card-body">
                  
                  

                    <div class="input-group">
                      
                    @foreach($users as $user)   
                        <!--  Kullanıcı-->
                       <div class="card-body" style=" border: 1px solid #d5e4f5;
  border-radius: 5px; margin-bottom:2px;">
                        <div class="row">
                            <div class="col-md-4">
                            @if(isset($user->image,$user->password))
                                <img src="/images/profile/{{$user->image}}" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                            @elseif($user->image == null)
                                <img src="/assets/img/newuserlogo.png" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                            @else
                             <img src="{{$user->image}}" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                            @endif
                             
                            </div>
                            <div class="col-md-8">
                                <p>
                                    <a style="color:black !important;" class="float-left" href="/profile/other/{{$user->id}}">{{$user->name}}</a>
                                  @php $takip = App\Follow::where('follower_id',$currentuser->id)->where('leader_id',$user->id)->first() @endphp
                                  
                                  @if($takip)  
                                    <form action="{{ route('unfollow', ['userId' => $user->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    <button  type="submit" class="btn btn-danger btn-sm">
                                        
                                        Takibi Bırak
                                    </button>
                                    </form>
                                  @else
                                  <form action="{{ route('follow', ['userId' => $user->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    <button  type="submit" class="btn btn-primary btn-sm">
                                        
                                        Takip Et 
                                    </button>
                                    </form>
                                  @endif  
                               </p>
                               <div class="clearfix"></div>
                               
                            </div>

                        </div>
                    </div>
                
                    @endforeach

                    </div>  
                </div>
              </div>
              <!-- Önerilen Kullanıcılar Son --> 
            
           
        </div>
      </div>
    </div>
 <!--durum Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
       <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Düzenle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
       </div>
    <div class="modal-body">
    
       <div class="card-body">


           <div id="carouselExampleSlidesOnly" class="carousel slide mb-3" data-ride="carousel">
               <div class="carousel-inner">
                 <div class="carousel-item active">
                     <img src="/assets/img/ornek1.jpg" class="d-block w-100" alt="...">
                 </div>
               </div>
             </div>
             
             <div class="input-group">
                 
                 <input type="text" value="With supporting text below as a natural lead-in to additional content." class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
               </div>
         </div>
         <button type="button" class="btn btn-secondary">Görsel Ekle</button>
    </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            <button type="button" class="btn btn-primary">Düzenle</button>
         </div>
       </div>
     </div>
   </div>
<!--durum Modal -->
<!-- düzenle modal dosya -->
<div class="modal fade" id="duzenleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Düzenle</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">

<div class="card-body">

<object style="    width: 100%; height:200px;;zoom: 3;" data="/assets/doc/belge.pdf" type="application/pdf">
<embed src="/assets/doc/belge.pdf" type="application/pdf" />
</object>


<div class="input-group">
<div class="input-group mb-3">
<div class="input-group-prepend">
 <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
</div>
<select class="custom-select" id="inputGroupSelect01">
 <option selected>Seçiniz...</option>
 <option value="1">Mühendislik Fakültesi</option>
 <option value="2">Güzel Sanatlar Fakültesi</option>
 <option value="3">Fen Edebiyat Fakültesi</option>
</select>
</div>
<div class="input-group mb-3">
 <div class="input-group-prepend">
   <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
 </div>
 <select class="custom-select" id="inputGroupSelect01">
   <option selected>Seçiniz...</option>
   <option value="1">Bilgisayar Mühendisliği</option>
   <option value="2">Endüstri Mühendisliği</option>
   <option value="3">Elektirik Mühendisliği</option>
 </select>
</div>
<div class="input-group mb-3">
   <div class="input-group-prepend">
     <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
   </div>
   <select class="custom-select" id="inputGroupSelect01">
     <option selected>Seçiniz...</option>
     <option value="1">1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="3">4</option>
   </select>
 </div>
<input type="text" value="With supporting text below as a natural lead-in to additional content." class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
</div>
</div>
<button type="button" class="btn btn-secondary">Dosya Ekle</button>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
<button type="button" class="btn btn-primary">Düzenle</button>
</div>
</div>
</div>
</div>
<!-- düzenle modal dosya -->  
@endsection
@section('scripts')

@endsection
 <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-brand-wrapper d-flex justify-content-center">
        <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  
          <a class="navbar-brand brand-logo" href="/admin"></a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="/assets/panel/images/logo-mini.svg" alt="logo"/></a>
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-sort-variant"></span>
          </button>
        </div>  
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav mr-lg-4 w-100">
          <li class="nav-item nav-search d-none d-lg-block w-100">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="search">
                  <i class="mdi mdi-magnify"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="Arama Yap..." aria-label="search" aria-describedby="search">
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
         
         
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
             
              <span class="nav-profile-name">{{ Sentinel::getUser()->first_name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a href="/admin/profile" class="dropdown-item">
                <i class="mdi mdi-settings text-primary"></i>
                Ayarlar
              </a>
              <a href="javascript:void(0)"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="dropdown-item">

                <i class="mdi mdi-logout text-primary"></i>
                Çıkış
              </a>
            </div>
            <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form> 
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
   
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="/admin">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Anasayfa</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#product" aria-expanded="false" aria-controls="product">
              <i class="mdi mdi mdi-responsive menu-icon"></i>
              <span class="menu-title">Proje Yönetimi</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="product">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/admin/e-ticaret/products/create">Ekle</a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/e-ticaret/products">Listele</a></li>
                 <li class="nav-item"> <a class="nav-link" href="/admin/e-ticaret/urun-kategorileri">Proje Kategorileri</a></li>
              </ul>
            </div>
          </li>

  

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-slide" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-circle-outline menu-icon"></i>
              <span class="menu-title">Slide</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-slide">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/admin/slide/create">Ekle</a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/slide">Listele</a></li>
                 </li>
              </ul>
            </div>
          </li>
  
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="mdi mdi-google-pages menu-icon"></i>
              <span class="menu-title">Sayfalar</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/admin/pages/create">Ekle </a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/pages">Listele</a></li>
                
               
              </ul>
            </div>
          </li>

             <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-circle-outline menu-icon"></i>
              <span class="menu-title">Bölümler</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/admin/blogs/create">Ekle</a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/blogs">Listele</a></li>
                 <li class="nav-item"> <a class="nav-link" href="/admin/blog-kategorileri">Bölüm Kategorileri</a></li>
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="false" aria-controls="settings">
              <i class="mdi mdi mdi-settings menu-icon"></i>
              <span class="menu-title">Ayarlar</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="settings">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/admin/settings/sosyal-medya">Sosyal Medya Bağlantıları </a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/settings/seo-ayarlari">Site Ayarları</a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/settings/google-analytics">Google Analytics</a></li>
                <li class="nav-item"> <a class="nav-link" href="/admin/settings/iletisim-ayarlari">İletişim Ayarları</a></li>
                
               
              </ul>
            </div>
          </li>
        
        </ul>
      </nav>
     

  <!-- plugins:js -->
  <script src="/assets/panel/vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="/assets/panel/vendors/chart.js/Chart.min.js"></script>
  <script src="/assets/panel/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="/assets/panel/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="/assets/panel/js/off-canvas.js"></script>
  <script src="/assets/panel/js/hoverable-collapse.js"></script>
  <script src="/assets/panel/js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="/assets/panel/js/dashboard.js"></script>
  <script src="/assets/panel/js/data-table.js"></script>
  <script src="/assets/panel/js/jquery.dataTables.js"></script>
  <script src="/assets/panel/js/dataTables.bootstrap4.js"></script>

<script src="/assets/panel/js/sweetalert.js"></script>

@if(session('success'))
    <script type="text/javascript">
        swal("Başarılı", "{{ session('success') }}", "success");
    </script>
@elseif(session('error'))
    <script type="text/javascript">
        swal("Başarısız", "{{ session('error') }}", "error");
    </script>
@endif

<script type="text/javascript">
        $(document).ready(function(){          

          $('[data-plugin="ckeditor"]').each(function(){
              var name = $(this).attr('name');
              CKEDITOR.replace(name, ckeditor_options);
          });
          
        
      });
</script>
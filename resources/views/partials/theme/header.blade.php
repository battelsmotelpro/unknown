<header>
        <div class="header-area ">
            
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="/">
                                        <img src="/assets/img/logo.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-7">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="/">Anasayfa</a></li>
                                            <li><a href="/hakkimizda">Hakkımızda </a></li>
                                            <li><a href="/iletisim">İletişim</a></li>
                                            @if(isset($currentuser))
                                                <li><a href="#">Hesabım<i class="ti-angle-down"></i></a>
                                                        <ul class="submenu">
                                                                <li><a href="/profil">Profil</a></li>
                                                                <li><a href="/profile/mesajlar">Mesajlar</a></li>
                                                                <li><a href="/ilanlarim/{{$currentuser->id}}">İlanlarım</a></li>
                                                                <li><a href="/favori-ilanlarim">Favoriler</a></li>
                                                        </ul>
                                                </li>
                                            @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="Appointment">
                                   
                                    
                                    <div class="book_btn d-none d-lg-block">
                                        @if(!Auth::check())
                                            <a  href="/login">Giriş Yap</a>
                                            <a  href="/register">Kayıt Ol</a>
                                        @else
                                            <a  href="/real-estate/create">İlan Ver</a>
                                            <a  href="/logout">Çıkış Yap</a>
                                        @endif 
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
  
 
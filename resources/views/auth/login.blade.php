<!DOCTYPE html>
<html lang="en">
<head>
	<title>Unknown Real Estate</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/assets_login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/assets_login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/assets_login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets_login/css/util.css">
	<link rel="stylesheet" type="text/css" href="/assets_login/css/main.css">
  <link rel="stylesheet" href="/assets_login/css/bootstrap-social.css" >
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(/assets_login/images/bg-01.jpg);">
					<span class="login100-form-title-1">
						Giriş Yap
					</span>
				</div>

				<form action="/login" method="post" class="login100-form validate-form">
					{{ csrf_field() }}
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Email</span>
						<input class="input100" type="email" name="email" placeholder="Email Adresiniz">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Şifre</span>
						<input class="input100" type="password" name="password" placeholder="Şifreniz">
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Beni Hatırla
							</label>
						</div>

						<div>
							<a href="#" class="txt1">
								Şifremi Unuttum?
							</a>
						</div>
					</div>

					<div style="margin-bottom:10px;" class="container-login100-form-btn">
						<button class="login100-form-btn">
							Giriş Yap
						</button>
					<a href="/register">
           			<button style="margin-left:10px;" class="login100-form-btn">
							Kayıt Ol
						</button>
					</div>
					</a>
          <div style="margin-bottom:5px;" class="col-md-8">
            <a href="/login/facebook" class="btn btn-block btn-social btn-facebook">
              <span class="fa fa-facebook"></span>
            Facebook ile giriş yap
            </a>
          </div>

          <div class="col-md-8">
            <a  href="/login/google" class="btn btn-block btn-social btn-google">
              <span class="fa fa-google"></span>
            Google ile giriş yap
            </a>
          </div>
 
				</form>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="/assets_login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/assets_login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/assets_login/vendor/bootstrap/js/popper.js"></script>
	<script src="/assets_login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/assets_login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/assets_login/vendor/daterangepicker/moment.min.js"></script>
	<script src="/assets_login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/assets_login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/assets_login/js/main.js"></script>

</body>
</html>
<!doctype html>
<html lang="en">

<head>
    @include('partials.panel.head')
    @yield('styles')
</head>
 	 <body>
 	  <div class="container-scroller">  
 	   <div class="container-fluid page-body-wrapper">     
        @include('partials.panel.header')
        @yield('content')        
        @include('partials.panel.footer')
        </div>
        @include('partials.panel.scripts')
        @yield('scripts')
      </div>

    </body>
</html>
<!doctype html>
<html lang="en">

<head>
    @include('partials.theme.head')
    @yield('styles')
</head>
 	<body>        
        
        @yield('content')        
    </body>
</html>
<!doctype html>
<html lang="tr" >

<head>
    @include('partials.theme.head')
    @yield('styles')
</head>
 	<body >
     
        @include('partials.theme.header')
       
  
        @yield('content')        
        @include('partials.theme.footer')

      
        @include('partials.theme.scripts')
        @yield('scripts')
   
    </body>
</html>
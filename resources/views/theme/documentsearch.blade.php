@extends('layouts.main')

@section('styles')
<style type="text/css">
       <style>
      body{
      background:#f5f8fa
      }
      @media (min-width: 1200px) { .container {
          max-width: 1200px;
      } }
      </style>
</style>
@endsection


@section('content')
   <div class="container" style="padding:3%;padding-left: 7%;padding-right: 7%;">
      
      <div class="row">
       <!-- Anasayfa Akış  -->
       <div class="col-8">
        <!-- Listelenen Kullanıcı -->
        <div class="card-footer text-muted">
         
        
         @forelse($shipments as $key=>$document)
          <!-- Ana Sayfa 1 İçerik -->
          <div class="card mb-3">
              <div class="card-header text-right">
                  <!-- 1 İçerik Açılır Menü -->
                    <button class="btn  dropdown-toggle dropdown dropdown-menu-left btn-group-sm " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="" aria-expanded="false">
                     ...
                    </button>
                  
                  <!-- 1 İçerik Açılır Menü Son -->
              </div>
              <!-- 1 İçerik Body -->
              <div class="card-body">
         
           
                      <object style="    width: 100%; height:200px;;zoom: 3;" data="/media/{{$document->media}}" type="application/pdf">
                                   <embed src="/media/{{$document->media}}" type="application/pdf" />
                      </object>
  
           
                <p class="card-text"> <span class="glyphicon glyphicon-pencil" ><i class="fa fa-search"></i></span>{{$document->description}}</p>
              </div>
              <!-- 1 İçerik Body Son -->

              <!-- 1 İçerik Footer -->

              <!-- İçerik Altı Butonlar -->
              <div class="card-footer text-muted">
               
                <div class="btn-group" role="group" style="width: 100%;" aria-label="Basic example">
                  <button type="button" class="btn btn-success">
                      <span class="glyphicon glyphicon-star" ></span>
                    Beğen</button>
                  <button type="button"  data-toggle="collapse" href="#document-comments{{$key+1}}"  class="btn btn-success">Yorum Yap</button>
                  <button type="button" class="btn btn-success">Paylaş</button>
                </div>
  
              </div>
               <!-- İçerik Altı Butonlar Son --> 

               <!-- Yorum Yap Buton Açılır Menü -->
             <div class="collapse" id="document-comments{{$key+1}}">
              <div class="card-footer text-muted">
               <!-- Yorum Yap -->
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Yorumunuz..." aria-label="Yorumunuz..." aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Yorum Yap</button>
                  </div>
                </div>
                <!-- Yorum Yap Son -->
              </div>
              <!-- Yapılan Yorumlar -->
              <div class="card-footer text-muted">
                 <!-- Yorumlar -->
                 <div class="card mb-3">
                    <!-- Yorum 1 -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col-md-2">
                                  <img src="img/pp.jpg" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                                  <p class="text-secondary text-center">15 Dakika Önce </p>
                              </div>
                              <div class="col-md-10">
                                  <p>
                                      <a class="float-left" href="#"><strong>Halil İbrahim EKİNCİ</strong></a>
                                 </p>
                                 <div class="clearfix"></div>
                                  <p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                              </div>
                          </div>
                      </div>
                       <!-- Yorum 1 Son -->
                       <hr>
                       <!-- Yorum 2 -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col-md-2">
                                  <img src="img/pp.jpg" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                                  <p class="text-secondary text-center">15 Dakika Önce </p>
                              </div>
                              <div class="col-md-10">
                                  <p>
                                      <a class="float-left" href="#"><strong>Halil İbrahim EKİNCİ</strong></a>
                                 </p>
                                 <div class="clearfix"></div>
                                  <p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                              </div>
                          </div>
                      </div>
                      <!-- Yorum 2 Son -->
                  </div>
                  <!-- Yorumlar Son -->
              </div>
              <!-- Yapılan Yorumlar Son -->
            </div>
            <!-- Yorum Yap Buton Açılır Menü Son -->

            <!-- 1 İçerik Footer -->
          </div>
        <!-- Ana Sayfa 1 İçerik Son -->
          @empty
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Hay aksi!</strong> Aradığın kriterlerde bir doküman bulamadık :(
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endforelse
         
       </div>
       <!-- Listelenen Kullanıcı Son -->
      </div>



        <!-- Anasayfa Akış Son -->

        <!-- Sağ Menü -->
        <div class="col-4">
        
            
              
           
        </div>
      </div>
    </div>
<!-- Sağ Menü Son -->
  


@endsection
@section('scripts')

@endsection
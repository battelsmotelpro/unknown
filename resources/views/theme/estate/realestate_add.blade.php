<!DOCTYPE html>
<html lang="en">
<head>
	<title>İlan Ekle</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="/estate/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/estate/css/util.css">
	<link rel="stylesheet" type="text/css" href="/estate/css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
    
		<div class="wrap-contact100">
			<form action="/real-estate" method="post" enctype="multipart/form-data" class="contact100-form validate-form">
			    {{ csrf_field() }}
				<span class="contact100-form-title">
				    İlan Ekle 
				</span>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
					<span class="label-input100">Başlık *</span>
					<input class="input100" required type="text" name="title" placeholder="İlan Başlığı">
				</div>

                <div class="wrap-input100 validate-input bg0 rs1-alert-validate" >
					<span class="label-input100">Açıklama</span>
					<textarea class="input100" name="description" placeholder="İlanla ilgili açıklama girebilirsiniz"></textarea>
				</div>
			
				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Email (e@a.x)">
					<span class="label-input100">İl *</span>
					<input class="input100" required type="text" name="city" placeholder="İlanın Bulunduğu il">
				</div>

                <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Email (e@a.x)">
					<span class="label-input100">İl *</span>
					<input class="input100" required type="text" name="town" placeholder="İlanın bulunduğu ilçe">
				</div>
				
				<div class="wrap-input100 bg1 rs1-wrap-input100">
					<span class="label-input100">Kat</span>
					<input class="input100" type="text" name="level" placeholder="Dairenin Bulunduğu Kat">
				</div>

                <div class="wrap-input100 bg1 rs1-wrap-input100">
					<span class="label-input100">Oda Sayısı</span>
					<input class="input100" type="text" name="room_count" placeholder="Oda Sayısı">
				</div>
				<div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
					<span class="label-input100">Konum</span>
					<input class="input100" required type="text" name="location" placeholder="İlanın konumu için iframe ekleyin">
				</div>
				<div class="wrap-input100 input100-select bg1">
					<span class="label-input100">Konut Tipi *</span>
					<div>
						<select  required class="js-select2" name="service">
                            <option value="4">Lütfen Seçim Yapınız</option>
							<option value="0">Ev</option>
							<option value="1">İş Yeri</option>
							<option value="2">Arsa</option>
						</select>
						<div class="dropDownSelect2"></div>
					</div>
				</div>
                
				<div class="w-full dis-none js-show-service">
					<div class="wrap-contact100-form-radio">
						<span class="label-input100">Evin Durumunu Belirtiniz *</span>

						<div class="contact100-form-radio m-t-15">
							<input class="input-radio100" id="radio1" type="radio" name="type_product" value="0" checked="checked">
							<label class="label-radio100" for="radio1">
								Kiralık
							</label>
						</div>

						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio2" type="radio" name="type_product" value="1">
							<label class="label-radio100" for="radio2">
								Satılık
							</label>
						</div>

					
					</div>
				</div>

        
				<div class="wrap-input100 bg1 rs1-wrap-input100 price">
						<span class="label-input100">Fiyat *</span>
						<input class="input100" type="text" required name="price" placeholder="Fiyat Bilgisi Giriniz">
				</div>
				
				<div class="wrap-input100 bg1 rs1-wrap-input100 price">
						<span class="label-input100">İlan Görseli *</span>
						<input class="input100" type="file" required name="image" placeholder="İlan Görseli Seçiniz">
				</div>

				<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						<span>
							Kaydet
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>



<!--===============================================================================================-->
	<script src="/estate/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/estate/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/estate/vendor/bootstrap/js/popper.js"></script>
	<script src="/estate/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/estate/vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
              
			});

          
			$(".js-select2").each(function(){
				$(this).on('select2:close', function (e){
                if($(this).val() == '0' && $(this).val() !== '4' ){
                    if($(this).val() == 'Ev') {
						$('.js-show-service').slideUp();
					}
					else {
                      
						$('.js-show-service').slideUp();
						$('.js-show-service').slideDown();
					}
                }
                else if($(this).val() !== '4'  && $(this).val() !== '0'){
                    $('.js-show-service').slideUp();
                   
                }
                else if($(this).val() == '4'){
                    $('.js-show-service').slideUp();
                   
                }
                   
				});
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="/estate/vendor/daterangepicker/moment.min.js"></script>
	<script src="/estate/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/estate/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/estate/vendor/noui/nouislider.min.js"></script>

<!--===============================================================================================-->
	<script src="/estate/js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>

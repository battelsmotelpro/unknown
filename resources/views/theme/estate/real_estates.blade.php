
@extends('layouts.main')
@section('styles')
@endsection

@section('content')
 
    <div class="bradcam_area bradcam_bg_1">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="bradcam_text text-center">
                                    @if(Request::is('filter'))
                                        <h3>İlan Filtreleme Sonuçları</h3>
                                    @endif
                                    @if(Request::is('search'))
                                        <h3>İlan Arama Sonuçları</h3>
                                    @endif
                                    @if(Request::is('ilanlarim/*'))
                                        <h3>İlanlarım ve Paylaşımlar</h3>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
    <!-- popular_property  -->
    <div class="popular_property">
        <div class="container">
            
            <div class="row">
            @forelse($real_estates as $estate)
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_property">
                        <div class="property_thumb">
                            <div class="property_tag">
                                   {{ $estate->type_product === 0 ? 'Kiralık' : 'Satılık' }}
                            </div>
                            <img src="/images/real_estate/{{$estate->image}}" alt="">
                        </div>
                        <div class="property_content">
                            <div class="main_pro">
                                    <h3><a href="/ilan-detay/{{$estate->id}}">{{ $estate->title }}</a></h3>
                                    <div class="mark_pro">
                                        <img src="/assets/img/svg_icon/location.svg" alt="">
                                        <span>{{ $estate->town.'/'.$estate->city }}</span>
                                    </div>
                                    <span class="amount">{{ $estate->price.' '.'₺' }} </span>
                            </div>
                        </div>
                        <div class="footer_pro">
                                <ul>
                                    <li>
                                        <div class="single_info_doc">
                                            <i class="fa fa-user"></i>
                                            <span>{{ $estate->user->name }}</span>
                                            @if($estate->user_id !== $currentuser->id)
                                                <i class="fa fa-share"></i>
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <div class="single_info_doc">
                                            <img src="/assets/img/svg_icon/bed.svg" alt="">
                                            <span>Kat {{ $estate->level }}</span>
                                        </div>
                                    </li>
                                 
                                </ul>
                            </div>
                    </div>
                </div>
            @empty
                <div class="alert alert-danger">
                    <strong>Hay Aksi! </strong> İlan Bulunamadı.
                </div>
            @endforelse
            </div>
          
        </div>
    </div>
    <!-- /popular_property  -->
    


    <!-- accordion  -->
    <div class="accordion_area">
            <div class="container">
                <div class="row align-items-center">
                        <div class="col-xl-6 col-lg-6">
                                <div class="faq_ask">
                                    <h3>Frequently ask</h3>
                                        <div id="accordion">
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                    Adieus who direct esteem <span>It esteems luckily?</span>
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                                        <div class="card-body">Esteem spirit temper too say adieus who direct esteem esteems luckily or picture placing drawing.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                    Who direct esteem It esteems?
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                        <div class="card-body">Esteem spirit temper too say adieus who direct esteem esteems luckily or picture placing drawing.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingThree">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                    Duis consectetur feugiat auctor?
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                                                        <div class="card-body">Esteem spirit temper too say adieus who direct esteem esteems luckily or picture placing drawing.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="accordion_thumb">
                            <img src="/assets//assets/img/banner/accordion.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- accordion  -->

  

   

  
@endsection

@section('scripts')

@endsection
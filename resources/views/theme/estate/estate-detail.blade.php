
@extends('layouts.main')
@section('styles')
<style>
.modal-backdrop.show {
    opacity: 0 !important;
}
.modal-backdrop {
    z-index: 0 !important;
   
}
</style>
@endsection

@section('content')
 
         <!-- bradcam_area  -->
         <div class="property_details_banner">
                <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-md-8 col-lg-6">
                            
                                <div class="comfortable_apartment">
                                    <h4>{{ $estate->title }}</h4>
                                    <p> <img src="/assets/img/svg_icon/location.svg" alt=""> {{ $estate->town.'/'.$estate->city }}</p>
                                    <div class="quality_quantity d-flex">
                                        <div class="single_quantity">
                                            <img src="/assets/img/svg_icon/color_bed.svg" alt="">
                                            <span>{{  'KAT :'.$estate->level  }}</span>
                                        </div>
                                        <div class="single_quantity">
                                            <img src="/assets/img/svg_icon/color_bath.svg" alt="">
                                            <span>{{ 'Oda Sayısı :'.$estate->room_count }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-4 col-lg-6">
                                <div class="prise_quantity">
                                    <h4 style="color:white;">
                                        <div class="single_quantity">
                                            @if($estate->user_id !== $currentuser->id)
                                                <span><button id="block-form-button" class="btn btn-danger"><i class="fa fa-ban"></i></button></span>
                                                @php $follower =  \App\Follow::where('follower_id',$currentuser->id)->where('leader_id',$estate->user->id)->first() @endphp
                                                @if(!isset($follower))
                                                    <span><button id="follow-form-button" class="btn btn-primary"><i class="fa fa-user-plus"></i></button></span>
                                                @else
                                                    <span><button id="unfollow-form-button" class="btn btn-danger"><i class="fa fa-undo"></i></button></span>
                                                @endif
                                                <span><button data-toggle="modal" data-target="#sendMessage" id="block-form-button" class="btn btn-primary"> <i class="fa fa-envelope"></i></i></button></span>

                                              
                                            @endif
                                     
                                        @if(!isset($estate->user->image))   
                                            <img style="width:50px;border-radius:10px; height:50px;" src="/assets/img/svg_icon/user.jpg" alt="">
                                        @else
                                        <img style="width:100px;border-radius:10px; height:100px;" src="/images/profile/{{$estate->user->image}}" alt="">
                                        @endif    
                                            <span>{{ $estate->user->name }}</span>
                                        </div>
                                    </h4>
                                    <h4>{{ $estate->price.' '.'₺' }}</h4>
                                    @if($currentuser->id === $estate->user->id)
                                        <a href="/real-estate/{{$estate->id}}/edit">Düzenle</a>
                                        <a href="#" id="estate-delete">Sil</a>
                                    @endif
                                    <a href="#" id="estate-shering">Paylaş</a>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
            <!--/ bradcam_area  -->

    <!-- details  -->
    <div class="property_details">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="property_banner">
                        <div class="property_banner_active owl-carousel">
                            <div class="single_property">
                                <img src="/images/real_estate/{{$estate->image}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="details_info">
                        <h4>Açıklama</h4>
                        {{ $estate->description }}
                    </div>
                    <section class="contact-section">
                            <div class="d-none d-sm-block">
                                   
                                {!!$estate->location!!}  
                    
                            </div>
                    </section>
                    <div class="contact_field">
                        <h3>Yorumlar</h3>
                       
                        @foreach($estate->comments as $comment)    
                        <form action="/comment/{{$comment->id}}" method="post">  
                            {{ csrf_field() }}
                                <div class="row">
                                        <div  class="col-xl-4 col-md-4">
                                            <div style="margin-bottom:5px;" class="single_quantity">
                                            
                                            @if(!isset($comment->user->image))      
                                               <img style="width:50px;border-radius:10px; height:50px;" src="/assets/img/svg_icon/user.jpg" alt="">
                                            @else
                                            <img style="width:50px;border-radius:10px; height:50px;" src="/images/profile/{{$comment->user->image}}" alt="">
                                            @endif    
                                                <span>{{ $comment->user->name }} </span>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div style="background:#1a6ca2 !important"  class="send_btn">
                                                <button style="background:#1a6ca2 !important"  type="submit" class="send_btn">{{ $comment->comment }}
                                                   
                                                </button>
                                            </div>
                                        </div>
                                        <div  class="col-xl-2 col-md-2">
                                            <div style="margin-bottom:5px; margin-top:4px;" class="single_quantity">
                                                @if($currentuser->id == $comment->user_id)
                                                    <button type="submit" id="delete-comment-button" class="btn btn-danger">  <i  class="fa fa-trash"></i></button> 
                                                    <a data-toggle="modal" data-target="#deleteComment-{{$comment->id}}" class="btn btn-success">  <i  class="fa fa-edit"></i></a> 

                                               @endif
                                            </div>
                                        </div>
                                      
                                </div>
                            </form>
                            <!-- Modal -->
                            <form action="/comment/{{$comment->id}}/edit" method="post">
                                {{ csrf_field() }}
			                   
                            <div class="modal fade" id="deleteComment-{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Yorum Düzenle</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <textarea name="comment" id="" cols="30" rows="10" placeholder="Yorumunuzu ekleyin..." >{{$comment->comment}}</textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            </form>
                        @endforeach        
                       
                    </div>
                    <div class="contact_field">
                        <h3>Yorum Yap</h3>
                        <form action="/comments" method="post">
                            {{ csrf_field() }}
                                <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                                <input type="hidden" name="estate_id" value="{{$estate->id}}">
                                <div class="row">
                                        <div class="col-xl-12">
                                           <textarea name="comment" id="" cols="30" rows="10" placeholder="Yorumunuzu ekleyin..." ></textarea>
                                        </div>
                                        <div class="col-xl-12">
                                            <div class="send_btn">
                                                <button type="submit" class="send_btn">Gönder</button>
                                            </div>
                                        </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /details  -->
    <!-- Modal -->
    <form action="/sendmessage" method="post">
        <input type="hidden" name="receiver_id" value="{{$estate->user->id}}">
        {{ csrf_field() }}
        <div class="modal fade" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $estate->user->name }} adlı kullanıcıya mesaj gönder </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-xl-12">
                    <textarea name="message" id="" cols="30" rows="10" placeholder="Mesaj" ></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-primary">Gönder</button>
            </div>
            </div>
        </div>
        </div>
    </form>
    <form action="{{url('real-estate')}}/{{$estate->id}}" method="post" id="delete-form">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
	</form>
    <form action="{{ route('estateShering', ['id' => $estate->id]) }}" id="shering-form" method="post">
                    {{ csrf_field() }}       
    </form>
    <form action="/profile/block/{{$estate->user_id}}" id="block-form" method="post">
        {{ csrf_field() }}       
    </form>
    <form action="/follow/{{$estate->user_id}}" id="follow-form" method="post">
        {{ csrf_field() }}       
    </form>
    <form action="/unfollow/{{$estate->user_id}}" id="unfollow-form" method="post">
        {{ csrf_field() }}       
    </form>
  
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function(){
    $("#estate-delete").click(function(){ 
        $("#delete-form").submit();
    });
    $("#estate-shering").click(function(){ 
        $("#shering-form").submit();
    });
    $("#block-form-button").click(function(){ 
        $("#block-form").submit();
    });
    $("#follow-form-button").click(function(){ 
        $("#follow-form").submit();
    });
    $("#unfollow-form-button").click(function(){ 
        $("#unfollow-form").submit();
    });
});
</script>
@endsection
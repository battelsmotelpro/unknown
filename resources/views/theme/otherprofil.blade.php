@extends('layouts.main')

@section('styles')
<style type="text/css">
       <style>
      body{
      background:#f5f8fa
      }
      @media (min-width: 1200px) { .container {
          max-width: 1200px;
      } }
      </style>
</style>
@endsection


@section('content')

  


    <div class="container" style="padding:3%;padding-left: 7%;padding-right: 7%;">
      
      <div class="row">
        <!-- Profil Bilgi Detaylar-->
        <div class="col-4">
          <div class="card center-block " style="padding: 5%;">
            <!-- Profil Foto  -->
            @if(isset($currentuser->image))
            <img src="/images/profile/{{$currentuser->image}}"  class="card-img-top rounded-circle img-fluid mb-3 "   style="width: 100%; height:100%;" alt="...">
            @else
            <img src="/assets/img/newuserlogo.png"  class="card-img-top rounded-circle img-fluid mb-3 "   style="width: 100%; height:100%;" alt="...">

            @endif
            <!-- İsim Soyisim  -->
            <a href="#" style="color: black; font-size: large;"  class="text-center ">{{ $currentuser->name }}</a>
            <!-- Profil İçerik  -->
            <div class="card-body text-center ">
          
              
                <!-- Hakkımda  -->
              <h5 class="card-title ">Hakkımda</h5>
               <!-- Kullanıcı hakkımızda alanını doldurduysa gösterilir doldurmadıysa varsayılan yazı gösterilir  -->
              @if(isset($currentuser->about))
               <p class="card-text text-left">{{$currentuser->about}}</p>
              
              @else
              <p class="card-text text-left">Profili düzenle diyerek kendini tanıtıcı yazılara yer verebilirsin </p>
              @endif
                <!-- Hakkımda Son  -->
            </div>
            <ul class="list-group list-group-flush">
              <!-- Profil Mail Adresi  -->
             <a href="/profile/followings"><li class="list-group-item">Takip Edilenler-({{$followings}})</li></a> 
              <a href="/profile/followers"><li class="list-group-item">Takip Edenler-({{$followers}})</li></a>
              <li class="list-group-item">{{$currentuser->email}}</li>
              <li class="list-group-item"><button data-toggle="modal" data-target="#sendmessage" class="btn btn-primary btn-sm">Mesaj Gönder</button></li>
              <form action="/profile/block/{{$currentuser->id}}" method="post">
                {{ csrf_field() }}
             <li class="list-group-item"><button onclick="return confirm('Kullanıcıyı engellemek istediğine emin misiniz? Kullanıcıyı ve paylaşımlarını göremeyeceksiniz...');" class="btn btn-danger btn-sm">Kullanıcıyı Engelle</button></li>
              </form>
            </ul>
           
            <!-- Profil İçerik Son  -->
          
          </div>
          
        </div>
        <!-- Profil Bilgi Detaylar Son -->
        <div class="col-8"> 
        

    <div style="display:none;" class="card mb-3">
              
              <div class="card gedf-card">
                 
                  <div class="card-body">
                     <form action="/shipments" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }} 
                      <div class="tab-content" id="myTabContent">
                          <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                          <input type="hidden" name="type_id" value="0">
                          <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                              <div class="form-group">
                                  <label class="sr-only" for="message">post</label>
                                  <textarea class="form-control mb-3" id="message" name="title" rows="3" placeholder="Neler Oluyor ?"></textarea>
                                  <div class="custom-file">
                                      <input type="file" required="" name="image" class="custom-file-input" id="customFile">
                                      <label class="custom-file-label" for="customFile">Resim Yükle</label>
                                  </div>
                              </div>
                              <button type="submit" class="btn btn-primary btn-sm">Gönder</button>
                          </div>
                        </form>  
                      
                          
                          <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                            
                              <form action="{{ route('documents')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                            <input type="hidden" name="type_id" value="1">
                              <div class="form-group">
                                  <div class="custom-file ">
                                      <input type="file" name="file" class="custom-file-input" id="customFile">
                                      <label class="custom-file-label" for="customFile">Dosya Ekle</label>
                                  </div>
                              </div>
                              <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
                                  </div>
                                  <select name="faculty_id" class="custom-select" id="inputGroupSelect01">
                                    <option selected>Seçiniz...</option>
                                    <option value="1">Mühendislik Fakültesi</option>
                                    <option value="2">Güzel Sanatlar Fakültesi</option>
                                    <option value="3">Fen Edebiyat Fakültesi</option>
                                  </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
                                    </div>
                                    <select name="chapter_id" class="custom-select" id="inputGroupSelect01">
                                      <option selected>Seçiniz...</option>
                                      <option value="1">Bilgisayar Mühendisliği</option>
                                      <option value="2">Endüstri Mühendisliği</option>
                                      <option value="3">Elektirik Mühendisliği</option>
                                    </select>
                                  </div>
                                  <div class="input-group mb-3">
                                      <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
                                      </div>
                                      <select name="class_id" class="custom-select" id="inputGroupSelect01">
                                        <option selected value="0">Seçiniz...</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="3">4</option>
                                      </select>
                                    </div>

                                    <textarea class="form-control" name="description" id="message" rows="3" placeholder="Açıklama..."></textarea>
                              <div class="py-4"></div>
                               <button type="submit" class="btn btn-primary btn-sm">Gönder</button>
                             </form>
                          </div>
                     
                      </div>
                     
                  </div>
              </div>
              
            
            </div>

<!--  -->


    @if(isset($currentuser->birthday))
        @if($birthday === $now)
    <div style="position: absolute;" class="text-center">
    <div style="z-index: 99999;" class="balloon"></div>
    <div  style="z-index: 999999;"class="balloon"></div>
    <div style="z-index: 999999;" class="balloon"></div>
    <div  style="z-index: 999999;"class="balloon"></div>
    <div style="z-index: 999999;" class="balloon"></div>
    </div>
        @endif
    @endif

        @if(Session::has('success'))
              <div class="alert alert-primary">
                {{ Session::get('success')}}
              </div>
              @endif
        <!-- Ana Sayfa 2 İçerik -->
        @foreach($currentuser->shipments as $key=>$shipment)
        <div class="card mb-3">
            <div class="card-header text-right">

               <!-- 2 İçerik Açılır Menü -->
                  <button class="btn  dropdown-toggle dropdown dropdown-menu-left btn-group-sm " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="" aria-expanded="false">
                   ...
                  </button>
                 
                 <!-- 2 İçerik Açılır Menü Son -->
            </div>
             <!-- 2 İçerik Body -->
                @if($shipment->media==null && $shipment->image == null)
              <div class="card-body">
           
                
              <p class="card-text"> <span class="glyphicon glyphicon-pencil" ></span>{{$shipment->title}}</p>
          
            </div>

             @endif
             @if(isset($shipment->media))
               <object style="    width: 100%; height:200px;;zoom: 3;" data="/media/{{$shipment->media}}" type="application/pdf">
                                   <embed src="/media/{{$shipment->media}}" type="application/pdf" />
                      </object>
  
           
                <p class="card-text"> <span class="glyphicon glyphicon-pencil" ><i class="fa fa-search"></i></span>{{$shipment->description}}</p>
             @endif
             @if(isset($shipment->image))
            <div class="card-body">
              <div id="carouselExampleSlidesOnly" class="carousel slide mb-3" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/images/shipments/{{$shipment->image}}" class="d-block w-100" alt="...">
                    </div>
                  </div>
                </div>
                
              <p class="card-text"> <span class="glyphicon glyphicon-pencil" ></span>{{$shipment->title}}</p>
          
            </div>
            <!-- 2 İçerik Body Son -->
              @endif

           <!-- 2 İçerik Footer -->

              <!-- İçerik Altı Butonlar -->
              <div class="card-footer text-muted">
               
                  <div class="btn-group" role="group" style="width: 100%;" aria-label="Basic example">
                    @php $countLike = App\ShipmentLike::where('shipment_id',$shipment->id)->where('user_id',$currentuser->id)->count() @endphp
                    
                    @php $like = App\ShipmentLike::where('shipment_id',$shipment->id)->where('user_id',$currentuser->id)->first() @endphp
                    @if($like)
                   
                    <button  type="text" class="btn btn-success">
                        <span class="fa fa-thumbs-up" ></span>
                        Beğendin <i>({{$countLike}})</i>
                    </button>
                   

                    @else
                    <form action="{{ route('shipmentLike', ['id' => $shipment->id]) }}" method="post">
                    {{ csrf_field() }}
                    <button  type="submit" class="btn btn-success">
                        <span class="fa fa-thumbs-up" ></span>
                        Beğen <i>({{$countLike}})</i>
                    </button>
                    </form>
                    @endif
                  

                    <button type="button"  data-toggle="collapse" href="#shipmentscomments{{$key+1}}"  class="btn btn-success">
                     <span class="fa fa-comments" ></span>
                    Yorum Yap</button>
                    
                    <button type="button" class="btn btn-success">
                    <span class="fa fa-share-alt" ></span>
                    Paylaş</button>
                  </div>
    
                </div>
                 <!-- İçerik Altı Butonlar Son --> 

                 <!-- Yorum Yap Buton Açılır Menü -->
               <div class="collapse" id="shipmentscomments{{$key+1}}">
                <div class="card-footer text-muted">
                 <!-- Yorum Yap -->
                 <form action="{{ route('comment')}}" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="user_id" value="{{$currentuser->id}}">
                  <input type="hidden" name="shipment_id" value="{{$shipment->id}}">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="description" placeholder="Yorumunuz..." aria-label="Yorumunuz..." aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Yorum Yap</button>
                    </div>
                  </div>
                  </form>
                  <!-- Yorum Yap Son -->
                </div>
                <!-- Yapılan Yorumlar -->
                <div class="card-footer text-muted">
               
                   <!-- Yorumlar -->
                    <div class="card mb-3">
                    @php $comments = App\Comment::where('shipment_id',$shipment->id)->get() @endphp
                    @forelse($comments as $comment)

                    @php $user = App\User::where('id',$comment->user_id)->first() @endphp
                      <!-- Yorum 1 -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    @if(isset($user->image))
                                    <img src="/images/profile/{{$user->image}}" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                                    @else
                                    <img src="/assets/img/newuserlogo.png" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                                    @endif
                                    <p class="text-secondary text-center">{{$comment->created_at->diff($end)->format('%H:%I')}} Dakika Önce </p>

                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <a class="float-left" href="#"><strong>{{$user->name}}</strong></a>
                                   </p>
                                   <div class="clearfix"></div>
                                    <p>{{$comment->description}}</p>
                                </div>
                            </div>
                        </div>
                         <!-- Yorum 1 Son -->
                         <hr>
                    @empty
                           <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                   
                                    <img src="/assets/img/newuserlogo.png" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                                   

                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <a class="float-left" href="#"><strong>Hiç Yorum Eklenmemiş</strong></a>
                                   </p>
                                   <div class="clearfix"></div>
                                    <p>İlk Yorumu siz ekleyin</p>
                                </div>
                            </div>
                        </div>
                         

                    @endforelse
                    </div>
                    <!-- Yorumlar Son -->
                </div>
                <!-- Yapılan Yorumlar Son -->
              </div>
              <!-- Yorum Yap Buton Açılır Menü Son -->

              <!-- 2 İçerik Footer -->
        </div>
    <!-- Ana Sayfa 2 İçerik Son -->


                        <!--durum Modal -->
            <form action="{{ route('edit-shipment', ['id' => $shipment->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal fade" id="edit-shipment{{$key+1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                 <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Düzenle</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">
              
                 <div class="card-body">


                       @if(isset($shipment->media))
                         <object style="    width: 100%; height:200px;;zoom: 3;" data="/media/{{$shipment->media}}" type="application/pdf">
                                             <embed src="/media/{{$shipment->media}}" type="application/pdf" />
                                </object>
            
                     
                          <p class="card-text"> <span class="glyphicon glyphicon-pencil" ><i class="fa fa-search"></i></span>{{$shipment->description}}</p>
                          <div class="input-group">
                          <div class="input-group mb-3">
                          <div class="input-group-prepend">
                           <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
                          </div>
                          <select name="faculty_id" class="custom-select" id="inputGroupSelect01">

                            @foreach($faculties as $faculty)

                            @if($faculty->id == $shipment->faculty_id)                            
                                <option selected value="{{$shipment->faculty_id}}">
                                @if(isset($shipment->faculty->name))
                                {{$shipment->faculty->name}}</option>
                                @endif
                            @else
                                <option value="{{$faculty->id}}">{{ $faculty->name }}</option>
                            @endif
                            @endforeach
                          </select>
                          </div>
                          <div class="input-group mb-3">
                           <div class="input-group-prepend">
                             <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
                           </div>
                           <select name="chapter_id" class="custom-select" id="inputGroupSelect01">
                            @foreach($sections as $sec)
                            @if($sec->id   == $shipment->chapter_id )
                                  <option selected value="{{$shipment->chapter_id}}">  
                              @if(isset($shipment->deparment->name))
                                  {{$shipment->deparment->name}}</option>
                              @endif
                            @else
                            <option value="{{$sec->id}}">{{ $sec->name }}</option>
                            @endif
                            @endforeach
                           </select>
                          </div>
                          <div class="input-group mb-3">
                             <div class="input-group-prepend">
                               <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
                             </div>
                             <select class="custom-select" id="inputGroupSelect01">
                               <option selected>Seçiniz...</option>
                               <option value="1">1</option>
                               <option value="2">2</option>
                               <option value="3">3</option>
                               <option value="3">4</option>
                             </select>
                           </div>
                          <input type="text" value="With supporting text below as a natural lead-in to additional content." class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                          </div>
                           <div class="custom-file ">
                                      <input type="file" name="file" class="custom-file-input" id="customFile">
                                      <label class="custom-file-label" for="customFile">Dosya Ekle</label>
                            </div>
                       @endif
                       @if(isset($shipment->image))
                      <div class="card-body">
                        <div id="carouselExampleSlidesOnly" class="carousel slide mb-3" data-ride="carousel">
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                  <img src="/images/shipments/{{$shipment->image}}" class="d-block w-100" alt="...">
                              </div>
                            </div>
                          </div>
                          
                       
                       <div class="input-group">
                           
                           <input type="text" name="title" value="{{$shipment->title}}" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                         </div>
                      </div>
                       <div class="custom-file">
                                      <input type="file"  name="image" class="custom-file-input" id="customFile">
                                      <label class="custom-file-label" for="customFile">Resim Yükle</label>
                        </div>
                      <!-- 2 İçerik Body Son -->
                        @endif
                       
                      
                   </div>
                 
                  
              </div>
                   <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                      <button type="submit" class="btn btn-primary">Düzenle</button>
                   </div>
                 </div>
               </div>
             </div>
             </form>
          <!--durum Modal -->
          @endforeach





  
     
    </div>

  <!--Modeller -->

<!-- düzenle modal dosya -->
<div class="modal fade" id="duzenleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Düzenle</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">

<div class="card-body">

<object style="    width: 100%; height:400px;;zoom: 3;" data="doc/belge.pdf" type="application/pdf">
<embed src="doc/belge.pdf" type="application/pdf" />
</object>


<div class="input-group">
<div class="input-group mb-3">
<div class="input-group-prepend">
 <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
</div>
<select class="custom-select" id="inputGroupSelect01">
 <option selected>Seçiniz...</option>
 <option value="1">Mühendislik Fakültesi</option>
 <option value="2">Güzel Sanatlar Fakültesi</option>
 <option value="3">Fen Edebiyat Fakültesi</option>
</select>
</div>
<div class="input-group mb-3">
 <div class="input-group-prepend">
   <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
 </div>
 <select class="custom-select" id="inputGroupSelect01">
   <option selected>Seçiniz...</option>
   <option value="1">Bilgisayar Mühendisliği</option>
   <option value="2">Endüstri Mühendisliği</option>
   <option value="3">Elektirik Mühendisliği</option>
 </select>
</div>
<div class="input-group mb-3">
   <div class="input-group-prepend">
     <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
   </div>
   <select class="custom-select" id="inputGroupSelect01">
     <option selected>Seçiniz...</option>
     <option value="1">1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="3">4</option>
   </select>
 </div>
<input type="text" value="With supporting text below as a natural lead-in to additional content." class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
</div>
</div>
<button type="button" class="btn btn-secondary">Dosya Ekle</button>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
<button type="button" class="btn btn-primary">Düzenle</button>
</div>
</div>
</div>
</div>
<!-- düzenle modal dosya -->  

<!-- profil düzenle modal  -->
<div class="modal fade" id="sendmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Mesaj Gönder</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
    
      <!-- Profil Düzenle Modal İçerik Giriş -->
    <div class="card-body">
         <!-- İsim İçerik Değiştir -->  
         <form action="/sendmessage" method="post" enctype="multipart/form-data">
         {{ csrf_field() }}
         <input type="hidden" name="receiver_id" value="{{$currentuser->id}}">
        <div class="input-group mb-3">
          
           
           <p>{{$currentuser->name}}  --</p> <p>adlı kullanıcıya mesaj gönder</p>
          </div>
         
      

          <!-- Hakımda İçerik Değiştir -->  
          <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Mesaj İçeriği</span>
              </div>
                  
              <textarea class="form-control" name="about" aria-label="With textarea"></textarea>
            </div>
         

        
                <!-- Resim Değiştir Son -->   
      <!-- Profil Düzenle Modal Butonlar -->
    <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
    <button type="submit" class="btn btn-primary">Gönder</button>
    </div>
    <!-- Profil Düzenle Modal Butonlar Son -->
    </form>
    </div>
      <!-- Profil Düzenle Modal İçerik Giriş Son-->
    </div>
    </div>
    <!--profil düzenle modal  -->  
    

    <!--Balonlar Style  -->  
    <style>

        .balloon {
          display:inline-block;
           width:120px;
          height:145px;
          background:hsl(215,50%,65%);
          border-radius:80%;
          position:relative;
          box-shadow:inset -10px -10px 0 rgba(0,0,0,0.07);
          margin:20px 30px;
          transition:transform 0.5s ease;
          z-index:10;
          animation:balloons 4s ease-in-out infinite;
          transform-origin:bottom center;
        }
        
        @keyframes balloons {
          0%,100%{ transform:translateY(0) rotate(-4deg); }
          50%{ transform:translateY(-25px) rotate(4deg); }
        }
        
        
        .balloon:before {
          content:"▲";
          font-size:20px;
          color:hsl(215,30%,50%);
          display:block;
          text-align:center;
          width:100%;
          position:absolute;
          bottom:-12px;
          z-index:-100;
        }
        
        .balloon:after {
         display:inline-block; top:153px;
          position:absolute;
          height:250px;
          width:1px;
          margin:0 auto;
          content:"";
          background:rgba(0,0,0,0.2); 
        }
        
        .balloon:nth-child(2){ background:hsl(245,40%,65%); animation-duration:3.5s; }
        .balloon:nth-child(2):before { color:hsl(245,40%,65%);  }
        
        .balloon:nth-child(3){ background:hsl(139,50%,60%); animation-duration:3s; }
        .balloon:nth-child(3):before { color:hsl(139,30%,50%);  }
        
        .balloon:nth-child(4){ background:hsl(59,50%,58%); animation-duration:4.5s; }
        .balloon:nth-child(4):before { color:hsl(59,30%,52%);  }
        
        .balloon:nth-child(5){ background:hsl(23,55%,57%); animation-duration:5s; }
        .balloon:nth-child(5):before { color:hsl(23,44%,46%);  }
        </style>
            <!--Balonlar Style Son -->  
@endsection
@section('scripts')

@endsection
@extends('layouts.main')

@section('styles')
<style type="text/css">
       <style>
      body{
      background:#f5f8fa
      }
      @media (min-width: 1200px) { .container {
          max-width: 1200px;
      } }
      </style>
</style>
@endsection


@section('content')
   <div class="container" style="padding:3%;padding-left: 7%;padding-right: 7%;">
      
      <div class="row">
       <!-- Anasayfa Akış  -->
       <div class="col-8">
        <!-- Listelenen Kullanıcı -->
        <div class="card-footer text-muted">
         
          <div class="card mb-3">
            @foreach($currentuser->followings as $user) 
             <!-- Kullanıcı 1 -->
               <div class="card-body">
                   <div class="row">
                       <div class="col-md-2">
                          @if(isset($user->image))
                                <img src="/images/profile/{{$user->image}}" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                          @else
                                <img src=" /assets/img/newuserlogo.png" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                          @endif
                           
                       </div>
                       <div class="col-md-10">
                           <p>
                               <a class="float-left" href="#"><strong>{{$user->name}}</strong></a>
                               <BR></BR>
                               <a href="#"  class="btn btn-danger btn-sm">Takibi Bırak</a>
                          </p>
                          <div class="clearfix"></div>
                         
                       </div>
                   </div>
               </div>
                <!-- Kullanıcı 1 Son -->
            
            @endforeach
           </div>
         
       </div>
       <!-- Listelenen Kullanıcı Son -->
      </div>



        <!-- Anasayfa Akış Son -->

        <!-- Sağ Menü -->
        <div class="col-4">
        
            <div class="card mb-3">
                <h5 class="card-header">Döküman Arama Sistemi</h5>
                <div class="card-body">
                   <!-- Döküman Listele Ara -->
                    <input class="form-control mr-sm-2 mb-3" type="search" placeholder="Ara" aria-label="Search">

                    <div class="input-group">
                       
                        <div class="input-group mb-3">
                        <div class="input-group-prepend">
                         <label class="input-group-text" for="inputGroupSelect01">Fakülte</label>
                        </div>
                        <select class="custom-select" id="inputGroupSelect01">
                         <option selected>Seçiniz...</option>
                         <option value="1">Mühendislik Fakültesi</option>
                         <option value="2">Güzel Sanatlar Fakültesi</option>
                         <option value="3">Fen Edebiyat Fakültesi</option>
                        </select>
                        </div>
                        <div class="input-group mb-3">
                         <div class="input-group-prepend">
                           <label class="input-group-text" for="inputGroupSelect01">Bölüm</label>
                         </div>
                         <select class="custom-select" id="inputGroupSelect01">
                           <option selected>Seçiniz...</option>
                           <option value="1">Bilgisayar Mühendisliği</option>
                           <option value="2">Endüstri Mühendisliği</option>
                           <option value="3">Elektirik Mühendisliği</option>
                         </select>
                        </div>
                        <div class="input-group mb-3">
                           <div class="input-group-prepend">
                             <label class="input-group-text" for="inputGroupSelect01">Sınıf</label>
                           </div>
                           <select class="custom-select" id="inputGroupSelect01">
                             <option selected>Seçiniz...</option>
                             <option value="1">1</option>
                             <option value="2">2</option>
                             <option value="3">3</option>
                             <option value="3">4</option>
                           </select>
                         </div>
                     
                        </div>

                        <a href="#" class="btn btn-primary btn-sm">Listele</a>
                </div>
              </div>
            <!-- Önerilen Kullanıcılar --> 
              <div class="card mb-3">
                <h5 class="card-header">Önerilen Kullanıcılar</h5>
                <div class="card-body">
                  
                  

                       <div class="input-group">
                       
                    @foreach($users as $user)   

                
                       <!--  Kullanıcı-->
                       <div class="card-body" style=" border: 1px solid #d5e4f5;
  border-radius: 5px; margin-bottom:2px;">
                        <div class="row">
                            <div class="col-md-4">
                            @if(isset($user->image))
                                <img src="/images/profile/{{$user->image}}" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                            @else
                                <img src=" /assets/img/newuserlogo.png" width="100%" class="img img-rounded img-fluid rounded-circle mb-3 "/>
                            @endif
                             
                            </div>
                            <div class="col-md-8">
                                <p>
                                    <a style="color:black !important;" class="float-left" href="#">{{$user->name}}</a>
                                  @php $takip = App\Follow::where('follower_id',$currentuser->id)->where('leader_id',$user->id)->first() @endphp
                                  
                                  @if($takip)  
                                     <a href="#" class="btn btn-danger btn-sm">Takip Edildi</a>
                                  @else
                                    <a href="{{ route('follow', ['id' => $user->id]) }}" class="btn btn-primary btn-sm">Takip Et</a>
                                  @endif  
                               </p>
                               <div class="clearfix"></div>
                               
                            </div>

                        </div>
                    </div>
                     <!--  Kullanıcı Son -->
                  
                    @endforeach

                    </div>    
                </div>
              </div>
              <!-- Önerilen Kullanıcılar Son -->
           
        </div>
      </div>
    </div>
<!-- Sağ Menü Son -->
  


@endsection
@section('scripts')

@endsection
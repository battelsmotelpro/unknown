
@extends('layouts.main')
@section('styles')
@endsection

@section('content')
    <!-- slider_area_start -->
    <div class="slider_area">
            <div class="single_slider  d-flex align-items-center slider_bg_1">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-10 offset-xl-1">
                                <div class="slider_text text-center justify-content-center">
                                    <h3>Find your best Property</h3>
                                    <p>Esteem spirit temper too say adieus who direct esteem.</p>
                                </div>
                                <div class="property_form">
                                    <form action="/filter"  method="post" id="filter-form">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="form_wrap d-flex">
                                                        <div class="single-field max_width ">
                                                                <label for="#">Lokasyon</label>
                                                                <select name="location" class="wide" >
                                                                        <option value="İstanbul" data-display="İstanbul">İstanbul</option>
                                                                        <option value="Ankara">Ankara</option>
                                                                        <option value="İzmir">İzmir</option>
                                                                </select>
                                                            </div>
                                                        <div class="single-field max_width ">
                                                                <label for="#">Konut Tipi</label>
                                                                <select name="service" class="wide" >
                                                                        <option  value="0" data-display="Konut">Konut</option>
                                                                        <option value="1">Arsa</option>
                                                                        <option value="2">İşyeri</option>
                                                                </select>
                                                            </div>
                                                          
                                                        <div class="single-field min_width ">
                                                                <label for="#">Oda Sayısı</label>
                                                                <select name="room_count" class="wide" >
                                                                        <option value="1" data-display="01">01</option>
                                                                        <option value="2">02</option>
                                                                        <option value="3">03</option>
                                                                        <option value="4">04</option>
                                                                        <option value="5">05</option>
                                                                        <option value="6">06</option>
                                                                </select>
                                                            </div>
                                                        <div class="single-field min_width ">
                                                                <label for="#">Kat</label>
                                                                <select name="level" class="wide" >
                                                                        <option value="1" data-display="01">01</option>
                                                                        <option value="2">02</option>
                                                                        <option value="3">03</option>
                                                                </select>
                                                            </div>
                                                            <div id="filter-button" class="serach_icon">
                                                                    <a style="margin-top: 29px;" href="#">
                                                                            <i  class="ti-search"></i>
                                                                    </a>
                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <!-- slider_area_end -->

    <!-- popular_property  -->
    <div class="popular_property">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title mb-40 text-center">
                        <h3>Populer İlanlar</h3> 
                        <div class="search_btn">
                        <form action="search" method="get"> 
                            <input type="text" placeholder="İlan Ara" name="search">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach($real_estates as $estate)
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_property">
                        <div class="property_thumb">
                            <div class="property_tag">
                                   {{ $estate->type_product === 0 ? 'Kiralık' : 'Satılık' }}
                            </div>
    
                            <img src="/images/real_estate/{{$estate->image}}" alt="">
                        </div>
                        <div class="property_content">
                            <div class="main_pro">
                                    <h3><a href="/ilan-detay/{{$estate->id}}">{{ $estate->title }}</a></h3>
                                    <div class="mark_pro">
                                        <img src="/assets/img/svg_icon/location.svg" alt="">
                                        <span>{{ $estate->town.'/'.$estate->city }}</span>
                                    </div>
                                    <span class="amount">{{ $estate->price.' '.'₺' }} </span>
                                    <a href="javascript:void(0)" onclick="event.preventDefault();
                                                     document.getElementById('estate-favorites-{{$estate->id}}').submit();">
                                    <span style="margin-left:166px;background:#f4945c !important" class="amount text-right"><i class="fa fa-star"></i></span>
                                    </a>
                                    <form action="{{ route('estateFavorite', ['id' => $estate->id]) }}" id="estate-favorites-{{$estate->id}}"  method="post">
                                        {{ csrf_field() }}  
                                    </form>
                            </div>
                        </div>
                        <div class="footer_pro">
                                <ul>
                                    <li>
                                        <div class="single_info_doc">
                                            <i class="fa fa-user"></i>
                                            <span>{{ $estate->user->name }}</span>
                                            
                                        </div>
                                    </li>
                                   @if(isset($currentuser))
                                    <li style="{{ $estate->user_id === $currentuser->id ? 'opacity:0.1;' : '' }}">
                                           <a href="javascript:void(0)" onclick="event.preventDefault();
                                                     document.getElementById('estate-shering-{{$estate->id}}').submit();">
                                            <div  class="single_info_doc">
                                                <i class="fa fa-share"></i>
                                                <span>Paylaş</span>
                                            </div>
                                            </a>
                                    </li>
                                    @endif
                                    <form action="{{ route('estateShering', ['id' => $estate->id]) }}" id="estate-shering-{{$estate->id}}"  method="post">
                                        {{ csrf_field() }}  
                                    </form>
                                    @php 
                                                $likes = \App\Like::where('estate_id',$estate->id)->get();
                                                $count = $likes->count();
                                            @endphp 
                                    <li>
                                        <a href="javascript:void(0)" onclick="event.preventDefault();
                                            document.getElementById('estate-like-{{$estate->id}}').submit();">
                                        <div class="single_info_doc">
                                            <i class="fa fa-thumbs-up"></i>
                                            <span>Beğen({{$count}})</span>
                                        </div>
                                        </a>
                                    </li>
                                    <form action="{{ route('estateLike', ['id' => $estate->id]) }}" id="estate-like-{{$estate->id}}"  method="post">
                                        {{ csrf_field() }}
                                    </form>
                                </ul>
                            </div>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="more_property_btn text-center">
                        <a href="#" class="boxed-btn3-line">More Properties</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /popular_property  -->
    


    <!-- accordion  -->
    <div class="accordion_area">
            <div class="container">
                <div class="row align-items-center">
                        <div class="col-xl-6 col-lg-6">
                                <div class="faq_ask">
                                    <h3>Sıkça Sorulan Sorular</h3>
                                        <div id="accordion">
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                Web sitesi mobil cihazlarla uyumlu mudur?


                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                                        <div class="card-body">Evet, tüm mobil cihazlarla uyumlu şekilde web siteniz görüntülenmektedir.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                  Profil bilgilerimi nasıl güncelleyebilirim?
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                        <div class="card-body">Siteye giriş yaptıktan sonra menü kısmından hesabım->profil diyerek profil bilgilerinize ulaşabilir ve güncelleyebilirsiniz.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingThree">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                    İlan verdikten sonra ilanı güncelleyebilir miyim ?
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                                                        <div class="card-body">İlanınınızı dilediğiniz zaman ilan detay sayfasına giderek düzenle butona tıkladıktan sonra düzenleyebilirsiniz.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="accordion_thumb">
                            <img src="/assets//assets/img/banner/accordion.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- accordion  -->
    
  

   

  
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    $("#estate-delete").click(function(){ 
        $("#delete-form").submit();
    });
    $("#estate-shering").click(function(){ 
        $("#shering-form").submit();
    });
    $("#filter-button").click(function(){ 
        $("#filter-form").submit();
    });
});
</script>
@endsection
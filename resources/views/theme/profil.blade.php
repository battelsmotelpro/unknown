@extends('layouts.main')

@section('styles')
<style>
.single-input {
    background: #1575b6 !important;
	border-radius:10px;
}
.single-textarea {
    background: #1475b5;
	border-radius:10px;
}
</style>
@endsection


@section('content')
 <!-- bradcam_area  -->
 <div class="bradcam_area bradcam_bg_1">
					<div class="container">
						<div class="row">
							<div class="col-xl-12">
								<div class="bradcam_text text-center">
									<h3>Profil</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ bradcam_area  -->
<!-- Start Align Area -->
<div class="whole-wrap">
		<div class="container box_1170">
			<div class="section-top-border">
				<h3 class="mb-30">{{$currentuser->name}}</h3>
				<div class="row">
					<div class="col-md-3">
					@if(!isset($currentuser->image))
						<img style="width:150px;border-radius:10px; height:150px;" src="/assets/img/svg_icon/user.jpg" alt="" class="img-fluid">
					@else
					<img style="width:150px;border-radius:10px; height:150px;" src="/images/profile/{{$currentuser->image}}" alt="" class="img-fluid">
					@endif
					</div>
					<div class="col-md-9 mt-sm-20">
         
						<h3 class="mb-30">Bilgileri Düzenle</h3>
						<form action="/profile/update" method="post"  enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="mt-10">
								<input type="text" name="name" placeholder="Ad Soyad"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ad Soyad'" required
									class="single-input" value="{{$currentuser->name}}">
							</div>
							
							<div class="mt-10">
								<input type="email" name="email" placeholder="Email adresi"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email adresi'" required
									class="single-input" value="{{$currentuser->email}}">
							</div>
							<div class="mt-10">
								<input type="text" name="phone" placeholder="Telefon"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Telefon'" required
									class="single-input" value="{{$currentuser->phone}}">
							</div>
							<div class="mt-10">
								<textarea class="single-textarea" name="bio" placeholder="Hakkında kısa bilgiler girebilirsin" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Hakkında kısa bilgiler girebilirsin'" required>{{$currentuser->bio}}</textarea>
							</div>
							       
						    <div class="mt-10">
								<div class="custom-file ">
									<input type="file" name="image"    class="custom-file-input" id="customFile">
									<label class="custom-file-label" for="customFile">Pofil Resmini Değiştir</label>
								</div>
							</div>
							<div class="mt-10">
								<button type="submit"  class="single-input btn btn-success" style="background: #64cc7a !important;">Kaydet</button>
							</div>
						</form>
				
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Align Area --> 
@endsection
@section('scripts')

@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentLike extends Model
{
       protected $guarded = ['id'];
       protected $table = 'shipmentlikes';
       public $timestamps= false;
}

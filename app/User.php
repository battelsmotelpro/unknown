<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function sherings()
    {
        return $this->hasMany('App\Shering', 'user_id');
    }

    public function realestates()
    {
        return $this->hasMany('App\RealEstate', 'user_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Message', 'receiver_id');
    }

    public function favorites()
    {
        return $this->hasMany('App\Favorite', 'user_id');
    }
    public function blocks()
    {
        return $this->hasMany('App\Block', 'user_id');
    }
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'leader_id', 'follower_id')->withTimestamps();
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
    }

    public function tokens()
    {
        return $this->hasMany(Token::class);
    }

    public function getPhoneNumber()
    {
        return $this->country_code.$this->phone;
    }

    public function birthday($value)
    {
        return Carbon::parse($value)->format('m/d');
    }

     public function setGoogle2faSecretAttribute($value)
    {
         $this->attributes['google2fa_secret'] = encrypt($value);
    }

    public function getGoogle2faSecretAttribute($value)
    {
        return decrypt($value);
    }
   



    protected $fillable = [
        'name', 'email', 'password','provider','provider_id','phone','bio','image','token','birthday','google2fa_secret',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','google2fa_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

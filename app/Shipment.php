<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
      protected $guarded = ['id'];

    public function likes()
    {
        return $this->belongsToMany('App\ShipmentLike', 'shipment_id');
    } 

   public function user(){
    	return $this->belongsTo('App\User','user_id');
    }  

    public function faculty(){
        return $this->belongsTo('App\Faculty', 'faculty_id');
    }
    public function deparment(){
        return $this->belongsTo('App\Section', 'chapter_id');
    }

    
}

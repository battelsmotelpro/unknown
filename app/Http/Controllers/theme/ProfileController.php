<?php

namespace App\Http\Controllers\theme;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\ShipmentLike;
use Carbon\Carbon;
use App\Message;
use App\Shipment;
use App\Shering;
use App\Block;
use App\User;
use ImageU;

class ProfileController extends Controller
{
    public function profil()
    {

        return view('theme.profil');
    }

    public function sendmessage(Request $request){
       
        $sender = Auth::user();
        $message = Message::create([
            'sender_id' => $sender->id,
            'receiver_id' =>$request->receiver_id,
            'message'=> $request->message,
        ]);
        $user_name = User::find($request->receiver_id)->name;
      
        return back()->with('success',"$user_name adlı kullanıcıya mesaj gönderildi");
    }

    public function messages(){

        $currentuser = Auth::user();
        return view('theme.chat',compact('currentuser'));
    }

    public function userBlock($id){
      
        $member = Auth::user();
        $user = User::find($id);

        if(! $user) {
         return redirect()->back()->with('error', 'Kullanıcı Bulunamadı.'); 
        }

        $user->followers()->detach($member->id);
        $block = Block::updateOrCreate([
            'user_id'  => $member->id,
            'block_id' => $user->id,
        ]);

        return redirect('/')->with('success','Kullanıcıyı engellediniz...');                                             

    }
    public function update(Request $request)
    {
        $user = Auth::user();
        $image = $request->file('image');

        if($image){
            $eskiResim = $user->image;            
            if($eskiResim)
            File::delete('images/profile/'. $eskiResim);
            $imageName = time().'.'.$image->getClientOriginalExtension();
            ImageU::make($image)->save('images/profile/'.$imageName);
          
            $data = array_merge(['image' => $imageName], $request->except(['image']) );
        }else{
            $data = $request->except(['image']);
        }
        $user->update($data);
       
        return back()->with('success','Bilgileriniz başarıyla güncellendi...');
    }
}

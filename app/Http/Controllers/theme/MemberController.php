<?php

namespace App\Http\Controllers\theme;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function setLogout(){
        
        Auth::logout();
        return redirect('/login');
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function followUser(int $userId)
    {
        $member = Auth::user();
        $user = User::find($userId);

        if(!$user) {
            return back()->with('error', 'Kullanıcı Bulunamadı.'); 
        }

        $user->followers()->attach($member->id);
        return redirect("/ilanlarim/$member->id")->with('success',"$user->name adlı kullanıcı takip edildi"); 
    }

    public function unFollowUser(int $userId)
    {

        $member = Auth::user();
        $user = User::find($userId);
        if(! $user) { 
            return redirect()->back()->with('error', 'Kullanıcı Bulunamadı.'); 
        }
        $user->followers()->detach($member->id);
        return redirect()->back()->with('success', "$user->name adlı kullanıcı takipten çıkartıldı...");


    }
}

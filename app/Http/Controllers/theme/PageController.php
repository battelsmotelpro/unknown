<?php

namespace App\Http\Controllers\theme;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\RealEstate;
use App\User;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    public function index()
    {
        $currentuser = Auth::user();
        $data =[];
        if(isset($currentuser->blocks)){
            foreach ($currentuser->blocks as $key => $block) {
                $data[] = $block->block_id;
             }
        }
        
        
        $real_estates = RealEstate::whereNotIn('user_id',$data)->get(); 
        
        return view('theme.index',compact('real_estates'));
    }

    public function aboutUs()
    {
             
       return view('theme.about');
    }


    public function contact()
    {
             
       return view('theme.contact');
    }

    public function estate_detail($id)
    {
       
       $estate = RealEstate::find($id); 

       return view('theme.estate.estate-detail',compact('estate'));
    }

    public function favorite_estates()
    {
        $currentuser = Auth::user();
       
        $data =[];
        foreach ($currentuser->favorites as $key => $favorite) {
           $data[] = $favorite->estate_id;
        }
        
       $real_estates = RealEstate::whereIn('id',$data)->get(); 
       
       return view('theme.estate.favorite_estates',compact('real_estates'));
    }

    public function anasayfa()
    {
        $currentuser = Auth::user();
        $data =[];
        foreach ($currentuser->blocks as $key => $block) {
           $data[] = $block->block_id;
        }
        $data[] += $currentuser->id;
       
        $shipments = Shipment::where('user_id',$currentuser->id)->orderBy('id','desc')->get();
        $users = User::orderBy('id','desc')->whereNotIn('id',$data)->limit(5)->get();
        return view('home',compact('users','currentuser','shipments'));
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

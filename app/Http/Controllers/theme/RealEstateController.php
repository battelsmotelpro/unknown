<?php

namespace App\Http\Controllers\theme;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\RealEstate;
use App\Shering;
use App\Favorite;
use App\Like;

class RealEstateController extends Controller
{
    private $estate;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function currentuser(){
        return Auth::user();
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('theme.estate.realestate_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if(!$this->currentuser()){
            return back()->with('error','Kullanıcı girişi yapmanız gerekmektedir');
        }
        $request->flash();
        $image = $request->file('image');
        $imageName = time(). $image->getClientOriginalName();
        $image->move('images/real_estate/', $imageName);
                
        $data = array_merge(['image' => $imageName,'user_id' => $this->currentuser()->id], $request->except(['image']) );
        $kaydet = RealEstate::create($data);
         
        return redirect('/')->with('success', 'İlan Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
            $sherings =[];
            foreach ($this->currentuser()->sherings as $key => $shering) {
                $sherings[] = $shering->shering_id;
            }

            foreach ($this->currentuser()->realestates as $key => $estate) {
                $sherings[] = $estate->id;
            }
    
            foreach ($this->currentuser()->followings as $key => $follow) {
                foreach($follow->realestates as $estate){
                    $sherings[] = $estate->id;
                }
            }
      
        $real_estates = RealEstate::whereIn('id',$sherings)->get();
       
        return view('theme.estate.real_estates',compact('real_estates'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estate = RealEstate::find($id);

        return view('theme.estate.realestate_edit',compact('estate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estate = RealEstate::find($id);
        $image = $request->file('image');
        if($image){
            $last_image = $estate->image;            
            File::delete('images/real_estate/'. $last_image);
            
            $imageName = time(). $image->getClientOriginalName();
            $image->move('images/real_estate/', $imageName);

            $data = array_merge(['image' => $imageName], $request->except(['image','_token','_method']) );
             
            RealEstate::where('id', $id)      
            ->update( $data );
        }else{
            $data = array_merge(['image' => $estate->image],$request->except(['image','_token','_method']) );
            $ok = RealEstate::where('id', $id)          
            ->update($data);
        }         
        
        return redirect("/ilan-detay/$estate->id")->with('success', 'İlan Başarıyla Düzenlendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          
          $estate = RealEstate::find($id);
          $last_image = $estate->image; 
          if($last_image){
             File::delete('images/real_estate/'.$last_image); 
          } 
          
          $estate = RealEstate::find($id)->delete();
          if($estate)
          {
              return redirect('/')->with('success', 'İlan Başarıyla Silindi');
          }
    }

    public function estateShering(Request $request,$id){

        $shering = Shering::updateOrcreate([
            'user_id' => $this->currentuser()->id,
            'shering_id'=>$id,
        ]);
        $user_id = $this->currentuser()->id;
        return redirect("/ilanlarim/$user_id")->with('success','Gönderiyi Paylaştınız...');
    }

    public function estateFavorite(Request $request,$id){
       
        if(isset($request->favorite) && $request->favorite == 0){
           
            $favorite = Favorite::where('estate_id',$id)->delete();
            return redirect('/favori-ilanlarim')->with('success','Gönderiyi Favorilerden Çıkardınız');
        }
        else{
            $favorite = Favorite::updateOrcreate([
                'user_id' => $this->currentuser()->id,
                'estate_id'=>$id,
            ]);
            $user_id = $this->currentuser()->id;
        }
       
        return redirect('/favori-ilanlarim')->with('success','Gönderiyi Favorilere Eklediniz...');
    }

    public function estateLike(Request $request,$id){
        
        $like = Like::updateOrcreate([
            'user_id' => $this->currentuser()->id,
            'estate_id'=>$id,
        ]);

        return back();
    }
}

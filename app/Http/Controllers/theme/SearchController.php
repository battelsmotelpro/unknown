<?php

namespace App\Http\Controllers\theme;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\RealEstate;

class SearchController extends Controller
{
    public function search(Request $request){
        
        $search = $request->search;
      
        $real_estates = RealEstate::where('title','like','%'.$search.'%')->orWhere('description','like','%'.$search.'%')->orWhere('city','like','%'.$search.'%')->orWhere('town','like','%'.$search.'%')->get();
        
        return view('theme.estate.real_estates',compact('real_estates'));
    
    }

    public function filter(Request $request){
      
        $estate = (new RealEstate)->newQuery();
        if ($request->has('location')) {
            $estate->where('city',$request->location);
        }
        if ($request->has('level')) {
            $estate->where('level', $request->level);
        }
        $real_estates = $estate->get();
       
        return view('theme.estate.real_estates',compact('real_estates'));

    }
}

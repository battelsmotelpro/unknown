<?php

namespace App\Http\Controllers\theme;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Shipment;
use App\Shering;
use App\ShipmentLike;
use ImageU;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        
        $request->flash();
        $this->validate($request, [
                'title' => 'required|max:191',
            ],
            [
                'title.required' => 'Lütfen başlık alanını boş bırakmayın...',
            ]); 


        // kullanıcının görsel ekleyip eklemediği kontrol ediliyor...
        $image = $request->file('image');
        if($image){

            // durum görseli veritabanına ve klasöre kaydeliliyor...
            $imageName = time().'.'.$image->getClientOriginalExtension();
            ImageU::make($image)->save('images/shipments/'.$imageName);
            
            $data = array_merge(['image' => $imageName], $request->except(['image']) );
        }else{
            $data = $request->except(['image']);
        }   

        Shipment::create($data); // data değişkeninde tutulan tüm veriler veritabanına kaydediliyor...
       
        return back()->with('success','Durum başarıyla paylaşıldı...');


    }

    public function document(Request $request)
    {
       
       
        $request->validate([
            'file' => 'mimes:pdf,xlx,csv|max:2048',
        ]);
  
        $fileName = time().'.'.$request->file->extension();  
        $request->file->move(public_path('media'), $fileName);
        $data = $request->except(['image','file']);
        $data = array_merge($data, ['media' => $fileName]);
    
        Shipment::create($data);

        return back()->with('success','Doküman başarıyla paylaşıldı.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->flash();
        

        $image = $request->file('image');
         if($image){

            //önceki resmi sil
            $resmiSil = Shipment::find($id);
            $eskiResim = $resmiSil->image;            
            File::delete('images/shipments/'. $eskiResim);
            //önceki resmi sil

            $imageName = time().'.'.$image->getClientOriginalExtension();
            ImageU::make($image)->save( 'images/shipments/'.$imageName );  
            //resim ismini post edilen array e ekle
            $data = array_merge(['image' => $imageName], $request->except(['image','_token']) );
             
            Shipment::where('id', $id)      
            ->update( $data );

            }else{
                $data = array_merge( $request->except(['image','_token']) );
                $ok = Shipment::where('id', $id)          
                ->update($data);
            }         
        
        return  back()->with('success', 'Durumunuzu başarıyla güncellediniz');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           //var olan resmi sil
            $resmiSil = Shipment::find($id);
            $eskiResim = $resmiSil->image; 
            if($eskiResim){
               File::delete('images/shipments/'. $eskiResim); 
            }
        

        $shipment = Shipment::find($id)->delete();
        if($shipment)
        {
            return back()->with('success', 'Paylaşımızınız silindi :(');
        }
    }

    public function shipmentlike(Request $request,$id){

      
        $user = Auth::user();
        $user_id= $user->id;

        $like = ShipmentLike::updateOrcreate([
        'user_id' => $user_id,
        'shipment_id'=>$id,
        ]);

        return back();
    }

    public function shipmentShering(Request $request,$id){

      
        $user = Auth::user();
        $shering = Shering::updateOrcreate([
        'user_id' => $user->id,
        'sharing_id'=>$id,
        ]);

        return redirect('/profil')->with('success','Göneriyi Paylaştınız...');
    }

    public function shipmentsapi(){

        $data = Shipment::get();
        return response()->json($data,200);
    }

    public function shipmentapi($id){

        $data = Shipment::find($id);
        return response()->json($data,200);
    }

    public function storeapi(Request $request)
    {
       
        // kullanıcının görsel ekleyip eklemediği kontrol ediliyor...
        $image = $request->file('image');
        if($image){

            // durum görseli veritabanına ve klasöre kaydeliliyor...
            $imageName = time().'.'.$image->getClientOriginalExtension();
            ImageU::make($image)->save('images/shipments/'.$imageName);
            
            $data = array_merge(['image' => $imageName], $request->except(['image']) );
        }else{
            $data = $request->except(['image']);
        }   

        $shipment = Shipment::create($data); // data değişkeninde tutulan tüm veriler veritabanına kaydediliyor...
       
        return response()->json($shipment,201);


    }

    public function updateapi(Request $request, $id){

        $shipment = Shipment::find($id);
        $shipment->update($request->all());
        return response()->json($shipment,200);
    }

    public function deleteapi($id)
    {
        //var olan resmi sil
        $resmiSil = Shipment::find($id);
        $eskiResim = $resmiSil->image; 
        if($eskiResim){
           File::delete('images/shipments/'. $eskiResim); 
        }
        

        $shipment = Shipment::find($id)->delete();
        if($shipment)
        {
            return response()->json('Paylaşım Silindi',204);
        }
    }
}

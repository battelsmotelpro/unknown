<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealEstate extends Model
{
    protected $table = 'real_estates';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function likes()
    {
        return $this->belongsToMany('App\ShipmentLike', 'estate_id');
    } 
    
    public function comments()
    {
        return $this->hasMany('App\Comment', 'estate_id');
    } 
    
}
